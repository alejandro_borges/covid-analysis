# Paquetes y funciones ----
require(tm)
require(tidytext)
require(stringi)
require(readxl)
require(lubridate)
require(zoo)
require(gridExtra)
require(ggthemes)
require(hrbrthemes)
require(magick)
require(scales)
require(RColorBrewer)
library(ggrepel)
require(readxl)
require(tidyverse)

rm_accent <- function(str,pattern="all") {
  if(!is.character(str))
    str <- as.character(str)

  pattern <- unique(pattern)

  if(any(pattern=="Ç"))
    pattern[pattern=="Ç"] <- "ç"

  symbols <- c(
    acute = "áéíóúÁÉÍÓÚýÝ",
    grave = "àèìòùÀÈÌÒÙ",
    circunflex = "âêîôûÂÊÎÔÛ",
    tilde = "ãõÃÕñÑ",
    umlaut = "äëïöüÄËÏÖÜÿ",
    cedil = "çÇ"
  )

  nudeSymbols <- c(
    acute = "aeiouAEIOUyY",
    grave = "aeiouAEIOU",
    circunflex = "aeiouAEIOU",
    tilde = "aoAOnN",
    umlaut = "aeiouAEIOUy",
    cedil = "cC"
  )

  accentTypes <- c("´","`","^","~","¨","ç")

  if(any(c("all","al","a","todos","t","to","tod","todo")%in%pattern)) # opcao retirar todos
    return(chartr(paste(symbols, collapse=""), paste(nudeSymbols, collapse=""), str))

  for(i in which(accentTypes%in%pattern))
    str <- chartr(symbols[i],nudeSymbols[i], str)

  return(str)
}

# Directorios ----
inp <- "MCCI/defs_cdmx/01_datos/"
out <- "MCCI/defs_cdmx/03_gráficas/"
fiuffi <- "Fuente: Elaboración de MCCI con información pública de la Secretaría de Salud y el Registro Civil de la CDMX"


# Datos ----
# Esta base de datos es la original; debido a que contiene datos
# personales no se compartirá.
d_actas <- read_excel(paste0(inp, "actas_defuncion_cdmx.xlsx"),
                      col_types = c("text", "date", "numeric",
                                    "numeric", "text", "text"
                                    # ,"text"
                                    ))

# Se requiere descargar la base de datos en "http://187.191.75.115/gobmx/salud/datos_abiertos/datos_abiertos_covid19.zip"
d_cov <- read_csv("./MCCI/defs_cdmx/01_datos/200519COVID19MEXICO.csv")


# Limpieza ----
# Así fue como MCCI limipió la base de datos origial

#Dado que no se cuenta con el original que incluye al juzgado, agregamos el campo
#juzgado como una secuencia
d_actas$juzgado<-seq_along(d_actas$ACTA)
data_actas <- d_actas %>%
  rename_all(tolower) %>%
  mutate_all(rm_accent) %>%
  mutate(
    fecha = as.Date.character(fecha, format = c("%Y-%m-%d")),
    edad = str_sub(edad, start = 1, end = 2),
    edad = as.numeric(edad),
    g_edad = case_when(
      edad<11 ~ "0 a 10 años",
      edad>10&edad<21 ~ "11 a 20 años",
      edad>20&edad<31 ~ "21 a 30 años",
      edad>30&edad<41 ~ "31 a 40 años",
      edad>40&edad<51 ~ "41 a 50 años",
      edad>50&edad<61 ~ "51 a 60 años",
      edad>60&edad<71 ~ "61 a 70 años",
      edad>70&edad<81 ~ "71 a 80 años",
      edad>80~ "80 años o más"
    ),
    razon = str_replace_all(razon, "[[:punct:]]", " "),
    razon = gsub("\\s+", " ", str_trim(razon)),
    razon = tolower(razon),
    covid = case_when(
      str_detect(razon, "covid") ~ 1,
      str_detect(razon, "19") ~ 1,
      str_detect(razon, "sars") ~ 1,
      str_detect(razon, " cov") ~ 1,
      str_detect(razon, "ncov") ~ 1,
      str_detect(razon, "nuevo coronavirus") ~ 1,
      str_detect(razon, "coronavirus") ~ 1,
      T ~ 0
    ),
    tipo_confirmacion = ifelse(
      str_detect(razon, "confi|posit"), "1. Confirmado", ifelse(
        str_detect(razon, "prob|posib|sospe|pb"), "2. Sospechoso", "3. No especificado"
      )
    ),
    comorbilidad = ifelse(
      str_detect(razon, "diab|asma|hipertens|renal|obesid|tabaquis|epoc|enfisema"), 1,0
    ),
    diabetes = ifelse(str_detect(razon, "diab"),1,0),
    asma_epoc_tab = ifelse(str_detect(razon, "asm|tabaquis|epoc|enfisema"),1,0),
    hipertension = ifelse(str_detect(razon, "hipertens"),1,0),
    renal = ifelse(str_detect(razon, "renal"),1,0),
    obesidad = ifelse(str_detect(razon, "obesid"),1,0),
    id_unico = paste0(
      juzgado, acta
    )
  ) %>%
  filter(covid==1) # %>% select(-c(covid,acta))

# openxlsx::write.xlsx(data_actas, paste0(inp, "01_data_actas_completa_limpia.xlsx"))

data_cov <- d_cov %>%
  # Las observaciones 09f6d9 y 07b5c5 presentan fecha_corte < 2020-02-28;
  # se asume que se trató de un error de captura y, para guardar consistencia,
  # cambian las fechas a marzo.
  mutate(
    FECHA_INGRESO =  lubridate::ymd(FECHA_INGRESO),
    FECHA_SINTOMAS = lubridate::ymd(FECHA_SINTOMAS),
    FECHA_INGRESO = case_when(
      ID_REGISTRO == "09f6d9" ~ lubridate::ymd(FECHA_INGRESO)+61,
      ID_REGISTRO == "07b5c5" ~ lubridate::ymd(FECHA_INGRESO)+31,
      T ~ FECHA_INGRESO
    ),
    FECHA_SINTOMAS = case_when(
      ID_REGISTRO == "09f6d9" ~ lubridate::ymd(FECHA_SINTOMAS)+61,
      ID_REGISTRO == "07b5c5" ~ lubridate::ymd(FECHA_SINTOMAS)+31,
      T ~ FECHA_SINTOMAS
    )
  ) %>%
  rename_all(tolower) %>%
  mutate(
    cve_inegi = paste0(entidad_res, municipio_res),
    origen = ifelse(
      origen==1, "USMER", ifelse(
        origen==2, "Fuera de USMER", NA
      )
    ),
    sector = case_when(
      sector == 1 ~ "Cruz Roja",
      sector == 2 ~ "DIF",
      sector == 3 ~ "Estatal",
      sector == 4 ~ "IMSS",
      sector == 5 ~ "IMSS-BIENESTAR",
      sector == 6 ~ "ISSSTE",
      sector == 7 ~ "Municipal",
      sector == 8 ~ "PEMEX",
      sector == 9 ~ "Privada",
      sector == 10 ~ "SEDENA",
      sector == 11 ~ "SEMAR",
      sector == 12 ~ "SSA",
      sector == 13 ~ "Universitario",
      T ~ NA_character_
    ),
    sexo = ifelse(
      sexo == 1, "Mujer", ifelse(
        sexo == 2, "Hombre", NA
      )
    ),
    g_edad = case_when(
      edad<11 ~ "0 a 10 años",
      edad>10&edad<21 ~ "11 a 20 años",
      edad>20&edad<31 ~ "21 a 30 años",
      edad>30&edad<41 ~ "31 a 40 años",
      edad>40&edad<51 ~ "41 a 50 años",
      edad>50&edad<61 ~ "51 a 60 años",
      edad>60&edad<71 ~ "61 a 70 años",
      edad>70&edad<81 ~ "71 a 80 años",
      edad>80~ "80 años o más"
    ),
    tipo_paciente = ifelse(
      tipo_paciente == 1, "Ambulatorio", ifelse(
        tipo_paciente == 2, "Hospitalizado", NA
      )
    ),
    nacionalidad = ifelse(
      nacionalidad == 1, "Mexicana", ifelse(
        nacionalidad == 2, "Extranjera", NA
      )
    ),
    resultado = ifelse(
      resultado == 1, "Positivo COVID-19", ifelse(
        resultado == 2, "No positivo COVID-19", "Pendiente"
      )
    ),
    intubado = ifelse(
      intubado == 1, "Si", ifelse(
        intubado == 2, "No", NA
      )
    ),
    neumonia = ifelse(
      neumonia == 1, "Si", ifelse(
        neumonia == 2, "No", NA
      )
    ),
    embarazo = ifelse(
      embarazo == 1, "Si", ifelse(
        embarazo == 2, "No", NA
      )
    ),
    habla_lengua_indig = ifelse(
      habla_lengua_indig == 1, "Si", ifelse(
        habla_lengua_indig == 2, "No", NA
      )
    ),
    diabetes = ifelse(
      diabetes == 1, "Si", ifelse(
        diabetes == 2, "No", NA
      )
    ),
    epoc = ifelse(
      epoc == 1, "Si", ifelse(
        epoc == 2, "No", NA
      )
    ),
    asma = ifelse(
      asma == 1, "Si", ifelse(
        asma == 2, "No", NA
      )
    ),
    inmusupr = ifelse(
      inmusupr == 1, "Si", ifelse(
        inmusupr == 2, "No", NA
      )
    ),
    hipertension = ifelse(
      hipertension == 1, "Si", ifelse(
        hipertension == 2, "No", NA
      )
    ),
    otra_com = ifelse(
      otra_com == 1, "Si", ifelse(
        otra_com == 2, "No", NA
      )
    ),
    cardiovascular = ifelse(
      cardiovascular == 1, "Si", ifelse(
        cardiovascular == 2, "No", NA
      )
    ),
    obesidad = ifelse(
      obesidad == 1, "Si", ifelse(
        obesidad == 2, "No", NA
      )
    ),
    renal_cronica = ifelse(
      renal_cronica == 1, "Si", ifelse(
        renal_cronica == 2, "No", NA
      )
    ),
    tabaquismo = ifelse(
      tabaquismo == 1, "Si", ifelse(
        tabaquismo == 2, "No", NA
      )
    ),
    otro_caso = ifelse(
      otro_caso == 1, "Si", ifelse(
        otro_caso == 2, "No", NA
      )
    ),
    migrante = ifelse(
      migrante == 1, "Si", ifelse(
        migrante == 2, "No", NA
      )
    ),
    uci = ifelse(
      uci == 1, "Si", ifelse(
        uci == 2, "No", NA
      )
    ),
    pais_nacionalidad = ifelse(
      pais_nacionalidad == 1, "Si", ifelse(
        pais_nacionalidad == 2, "No", NA
      )
    ),
    defuncion = ifelse(
      is.na(fecha_def), 0, 1
    )
  ) %>%
  rename(
    fecha_corte = fecha_ingreso,
    clave_estado = entidad_um
  ) %>%
  filter(
    clave_estado=="09" & defuncion ==1
  ) %>%
  filter(!str_detect(resultado, "No")) %>%
  mutate(
    resultado = ifelse(
      str_detect(resultado, "Pend"), "2. Sospechoso", "1. Confirmado"
    )
  ) %>%
  filter(fecha_def<as.Date("2020-05-13"))

# Tablas ----
openxlsx::write.xlsx(data_actas %>%
                       count(tipo_confirmacion) %>%
                       mutate(prop = n/sum(n)*100),
                     paste0(out, "tablas/01_cdmx_por_tipo_confirmacion.xlsx"))


openxlsx::write.xlsx(data_actas %>%
                       count(juzgado) %>%
                       mutate(prop = n/sum(n)*100),
                     paste0(out, "tablas/02_cdmx_por_juzgado.xlsx"))


openxlsx::write.xlsx(data_actas %>%
                       select(diabetes) %>% filter(diabetes==1) %>%
                       count() %>% mutate(comorbilidad = "1. Diabetes") %>%
                       bind_rows(
                         data_actas %>%
                           select(asma_epoc_tab) %>% filter(asma_epoc_tab==1) %>%
                           count()%>% mutate(comorbilidad = "5. Asma, EPOC o\ntabaquismo")
                       ) %>%
                       bind_rows(
                         data_actas %>%
                           select(hipertension) %>% filter(hipertension==1) %>%
                           count()%>% mutate(comorbilidad = "2. Hipertensión")
                       ) %>%
                       bind_rows(
                         data_actas %>%
                           select(renal) %>% filter(renal==1) %>%
                           count()%>% mutate(comorbilidad = "4. Padecimiento renal")
                       ) %>%
                       bind_rows(
                         data_actas %>%
                           select(obesidad) %>% filter(obesidad==1) %>%
                           count()%>% mutate(comorbilidad = "3. Obesidad")
                       ) %>%
                       bind_rows(
                         data_actas %>%
                           mutate(
                             comorbilidad = case_when(
                               diabetes == 1 ~ "con",
                               asma_epoc_tab == 1 ~ "con",
                               hipertension == 1 ~ "con",
                               renal == 1 ~ "con",
                               obesidad == 1 ~ "con",
                               T ~"No se especifica\ncomorbilidad"
                             )
                           ) %>% filter(comorbilidad=="No se especifica\ncomorbilidad") %>%
                           count() %>% mutate(comorbilidad="No se especifica\ncomorbilidad")
                       ) %>%
                       group_by(comorbilidad) %>% summarise_all(sum) %>%
                       mutate(prop = n/sum(n)*100),
                     paste0(out, "tablas/03_cdmx_por_comorbilidad.xlsx"))


openxlsx::write.xlsx(data_actas %>%
                       mutate(
                         comorbilidad_no_especificada = case_when(
                           diabetes == 1 ~ 0,
                           asma_epoc_tab == 1 ~ 0,
                           hipertension == 1 ~ 0,
                           renal == 1 ~ 0,
                           obesidad == 1 ~ 0,
                           T ~ 1
                         )
                       ) %>%
                       select(tipo_confirmacion, diabetes, asma_epoc_tab, hipertension, renal, obesidad, comorbilidad_no_especificada) %>%
                       group_by(tipo_confirmacion) %>%
                       summarise_all(funs(sum(.,na.rm=T))) %>%
                       pivot_longer(
                         cols = diabetes:comorbilidad_no_especificada,
                         names_to = "tipo_comorbilidad",
                         values_to = "n"
                       ) %>%
                       mutate(
                         tipo_comorbilidad = case_when(
                           str_detect(tipo_comorbilidad, "diab") ~ "1. Diabetes",
                           str_detect(tipo_comorbilidad, "asma") ~ "5. Asma, EPOC o\ntabaquismo",
                           str_detect(tipo_comorbilidad, "hipertens") ~ "2. Hipertensión",
                           str_detect(tipo_comorbilidad, "renal") ~ "4. Padecimiento renal",
                           str_detect(tipo_comorbilidad, "obesid") ~ "3. Obesidad",
                           T ~ "No se especifica\ncomorbilidad"
                         )
                       ),
                     paste0(out, "tablas/04_cdmx_por_tipo_confirmacion_comorbilidad.xlsx"))

openxlsx::write.xlsx(data_cov %>%
                       count(resultado) %>%
                       mutate(prop = n/sum(n)*100),
                     paste0(out, "tablas/01_ssalud_por_tipo_confirmacion.xlsx"))


openxlsx::write.xlsx(data_cov %>%
                       select(diabetes) %>% filter(diabetes=="Si") %>%
                       count() %>% mutate(comorbilidad = "1. Diabetes") %>%
                       bind_rows(
                         data_cov %>%
                           select(asma) %>% filter(asma=="Si") %>%
                           count()%>% mutate(comorbilidad = "5. Asma, EPOC o\ntabaquismo")
                       ) %>%
                       bind_rows(
                         data_cov %>%
                           select(hipertension) %>% filter(hipertension=="Si") %>%
                           count()%>% mutate(comorbilidad = "2. Hipertensión")
                       ) %>%
                       bind_rows(
                         data_cov %>%
                           select(renal_cronica) %>% filter(renal_cronica=="Si") %>%
                           count()%>% mutate(comorbilidad = "4. Padecimiento renal")
                       ) %>%
                       bind_rows(
                         data_cov %>%
                           select(obesidad) %>% filter(obesidad=="Si") %>%
                           count()%>% mutate(comorbilidad = "3. Obesidad")
                       ) %>%
                       bind_rows(
                         data_cov %>%
                           select(tabaquismo) %>% filter(tabaquismo=="Si") %>%
                           count()%>% mutate(comorbilidad = "5. Asma, EPOC o\ntabaquismo")
                       ) %>%
                       bind_rows(
                         data_cov %>%
                           select(epoc) %>% filter(epoc=="Si") %>%
                           count()%>% mutate(comorbilidad = "5. Asma, EPOC o\ntabaquismo")
                       ) %>%
                       bind_rows(
                         data_cov %>%
                           mutate(
                             comorbilidad = case_when(
                               diabetes == "Si" ~ "con",
                               asma == "Si" ~ "con",
                               hipertension == "Si" ~ "con",
                               renal_cronica == "Si" ~ "con",
                               obesidad == "Si" ~ "con",
                               tabaquismo == "Si" ~ "con",
                               epoc == "Si" ~ "con",
                               cardiovascular == "Si" ~ "con",
                               inmusupr == "Si" ~ "con",
                               T ~"No se especifica\ncomorbilidad"
                             )
                           ) %>% filter(comorbilidad=="No se especifica\ncomorbilidad") %>%
                           count() %>% mutate(comorbilidad="No se especifica\ncomorbilidad")
                       ) %>%
                       group_by(comorbilidad) %>% summarise_all(sum) %>%
                       mutate(prop = n/sum(n)*100),
                     paste0(out, "tablas/03_ssalud_por_comorbilidad.xlsx"))


openxlsx::write.xlsx(data_cov %>%
                       mutate(
                         comorbilidad_no_especificada = case_when(
                           diabetes == "Si" ~ "Si",
                           asma == "Si" ~ "Si",
                           hipertension == "Si" ~ "Si",
                           renal_cronica == "Si" ~ "Si",
                           obesidad == "Si" ~ "Si",
                           tabaquismo == "Si" ~ "Si",
                           epoc == "Si" ~ "Si",
                           cardiovascular == "Si" ~ "Si",
                           inmusupr == "Si" ~ "Si",
                           T ~"No se especifica\ncomorbilidad"
                         )
                       ) %>%
                       select(resultado, diabetes, asma, hipertension, renal_cronica, obesidad, tabaquismo, epoc, comorbilidad_no_especificada) %>%
                       mutate_at(.vars = vars(c(diabetes, asma, hipertension, renal_cronica, obesidad, tabaquismo, epoc, comorbilidad_no_especificada)),
                                 .funs = funs(ifelse(.=="Si",1,0))) %>%
                       group_by(resultado) %>%
                       summarise_all(funs(sum(.,na.rm=T))) %>%
                       pivot_longer(
                         cols = diabetes:comorbilidad_no_especificada,
                         names_to = "tipo_comorbilidad",
                         values_to = "n"
                       ) %>%
                       mutate(
                         tipo_comorbilidad = case_when(
                           str_detect(tipo_comorbilidad, "diab") ~ "1. Diabetes",
                           str_detect(tipo_comorbilidad, "asma") ~ "5. Asma, EPOC o\ntabaquismo",
                           str_detect(tipo_comorbilidad, "hipertens") ~ "2. Hipertensión",
                           str_detect(tipo_comorbilidad, "renal") ~ "4. Padecimiento renal",
                           str_detect(tipo_comorbilidad, "obesid") ~ "3. Obesidad",
                           str_detect(tipo_comorbilidad, "tabaquis") ~ "5. Asma, EPOC o\ntabaquismo",
                           str_detect(tipo_comorbilidad, "epoc|enfisema") ~ "5. Asma, EPOC o\ntabaquismo",
                           T ~ "No se especifica\ncomorbilidad"
                         )
                       ) %>%
                       group_by(resultado, tipo_comorbilidad) %>% summarise(n = sum(n)) %>%
                       mutate(prop = n/sum(n)*100),
                     paste0(out, "tablas/04_ssalud_por_tipo_confirmacion_comorbilidad.xlsx"))






# Gráficas comparativas ----
# Confirmados y sospechosos (SSalud vs Actas defunción CDMX)
tempo <- data_actas %>%
  mutate(tipo_confirmacion = ifelse(str_detect(tipo_confirmacion, "3"), "1. Confirmado", tipo_confirmacion)) %>%
  count(tipo_confirmacion) %>%
  mutate(prop = n/sum(n)*100,
         tipo = "Actas defunción CDMX") %>%
  bind_rows(
    data_cov %>%
      count(resultado) %>%
      mutate(prop = n/sum(n)*100,
             tipo = "Base de datos SSalud") %>%
      rename(tipo_confirmacion = resultado)
  ) %>% select(-prop)

fiuf <- "Comparación entre defunciones publicadas por la Secretaría de Salud (federal) y las actas de defunción de la CDMX"
fiuff <- "Aquellas actas que no contuvieran una palabra asociada con 'sospechoso', 'probable' o 'posible', se contabilizaron como casos confirmados."
ggplot() +
  geom_point(data = tempo,
             aes(
               x=n,
               y= tipo_confirmacion,
               col = tipo
             ), size = 6) +
  ggalt::geom_dumbbell(data = tempo %>% pivot_wider(names_from = tipo, values_from = n),
                       aes(
                         x=`Base de datos SSalud`,
                         xend=`Actas defunción CDMX`,
                         y= tipo_confirmacion,
                         yend= tipo_confirmacion
                       ),
                       colour_x="#edae52", colour_xend = "#9fb059",
                       size_x = 6, size_xend = 6,
                       dot_guide = T,
                       colour = "gray", size = 2) +
  scale_color_manual(name = "", values = c("#edae52", "#9fb059"),
                     labels = c("Base de datos SSalud", "Actas defunción CDMX")) +
  scale_x_continuous(breaks = seq(0,3300,200)) +
  labs(title = str_wrap(fiuf,75),
       subtitle = str_wrap(fiuff,130),
       caption = fiuffi,
       x = "",
       y = "") +
  theme_minimal()+
  theme(plot.title = element_text(size = 32, face = "bold"),
        plot.subtitle = element_text(size = 20),
        plot.caption = element_text(size = 20),
        strip.text = element_text(size = 15),
        panel.spacing.x = unit(3, "lines"),
        text = element_text(family = "Arial Narrow"),
        axis.text.x = element_text(size = 12, angle = 90),
        axis.text.y = element_text(size = 18),
        legend.position = "top",
        legend.text = element_text(size=18))
ggsave(filename = paste0(out, "01.png"),
       width = 15, height = 10, dpi = 100)


tempo <- data_actas %>%
  # mutate(tipo_confirmacion = ifelse(str_detect(tipo_confirmacion, "3"), "1. Confirmado", tipo_confirmacion)) %>%
  count(tipo_confirmacion) %>%
  mutate(prop = n/sum(n)*100,
         tipo = "Actas defunción CDMX") %>%
  bind_rows(
    data_cov %>%
      count(resultado) %>%
      mutate(prop = n/sum(n)*100,
             tipo = "Base de datos SSalud") %>%
      rename(tipo_confirmacion = resultado)
  ) %>% select(-prop) %>% filter(!str_detect(tipo_confirmacion, "3"))
fiuff <- "Aquellas actas que explícitamente no definieran si la defunción se trataba de un caso confirmado o sospechoso por COVID-19, fueron descartadas del conteo."
ggplot() +
  geom_point(data = tempo,
             aes(
               x=n,
               y= tipo_confirmacion,
               col = tipo
             ), size = 6) +
  ggalt::geom_dumbbell(data = tempo %>% pivot_wider(names_from = tipo, values_from = n),
                       aes(
                         x=`Base de datos SSalud`,
                         xend=`Actas defunción CDMX`,
                         y= tipo_confirmacion,
                         yend= tipo_confirmacion
                       ),
                       colour_x="#edae52", colour_xend = "#9fb059",
                       size_x = 6, size_xend = 6,
                       dot_guide = T,
                       colour = "gray", size = 2) +
  scale_color_manual(name = "", values = c("#edae52", "#9fb059"),
                     labels = c("Base de datos SSalud", "Actas defunción CDMX")) +
  scale_x_continuous(breaks = seq(0,3300,200)) +
  labs(title = str_wrap(fiuf,75),
       subtitle = str_wrap(fiuff,125),
       caption = fiuffi,
       x = "",
       y = "") +
  theme_minimal()+
  theme(plot.title = element_text(size = 32, face = "bold"),
        plot.subtitle = element_text(size = 20),
        plot.caption = element_text(size = 20),
        panel.spacing.x = unit(3, "lines"),
        text = element_text(family = "Arial Narrow"),
        axis.text.x = element_text(size = 12, angle = 90),
        axis.text.y = element_text(size = 18),
        legend.position = "top",
        legend.text = element_text(size=18))
ggsave(filename = paste0(out, "02.png"),
       width = 15, height = 10, dpi = 100)



tempo <- data_actas %>%
  mutate(tipo_confirmacion = ifelse(str_detect(tipo_confirmacion, "3"), "1. Confirmado", tipo_confirmacion)) %>%
  mutate(
    comorbilidad_no_especificada = case_when(
      diabetes == 1 ~ 0,
      asma_epoc_tab == 1 ~ 0,
      hipertension == 1 ~ 0,
      renal == 1 ~ 0,
      obesidad == 1 ~ 0,
      T ~ 1
    )
  ) %>%
  select(tipo_confirmacion, diabetes, asma_epoc_tab, hipertension, renal, obesidad, comorbilidad_no_especificada) %>%
  group_by(tipo_confirmacion) %>%
  summarise_all(funs(sum(.,na.rm=T))) %>%
  pivot_longer(
    cols = diabetes:comorbilidad_no_especificada,
    names_to = "tipo_comorbilidad",
    values_to = "n"
  ) %>%
  mutate(
    tipo_comorbilidad = case_when(
      str_detect(tipo_comorbilidad, "diab") ~ "1. Diabetes",
      str_detect(tipo_comorbilidad, "asma") ~ "5. Asma, EPOC o\ntabaquismo",
      str_detect(tipo_comorbilidad, "hipertens") ~ "2. Hipertensión",
      str_detect(tipo_comorbilidad, "renal") ~ "4. Padecimiento renal",
      str_detect(tipo_comorbilidad, "obesid") ~ "3. Obesidad",
      T ~ "No se especifica\ncomorbilidad"
    ),
    tipo = "Actas defunción CDMX"
  ) %>%
  bind_rows(
    data_cov %>%
      mutate(
        comorbilidad_no_especificada = case_when(
          diabetes == "Si" ~ "Si",
          asma == "Si" ~ "Si",
          hipertension == "Si" ~ "Si",
          renal_cronica == "Si" ~ "Si",
          obesidad == "Si" ~ "Si",
          tabaquismo == "Si" ~ "Si",
          epoc == "Si" ~ "Si",
          cardiovascular == "Si" ~ "Si",
          inmusupr == "Si" ~ "Si",
          T ~"No se especifica\ncomorbilidad"
        )
      ) %>%
      select(resultado, diabetes, asma, hipertension, renal_cronica, obesidad, tabaquismo, epoc, comorbilidad_no_especificada) %>%
      mutate_at(.vars = vars(c(diabetes, asma, hipertension, renal_cronica, obesidad, tabaquismo, epoc, comorbilidad_no_especificada)),
                .funs = funs(ifelse(.=="Si",1,0))) %>%
      group_by(resultado) %>%
      summarise_all(funs(sum(.,na.rm=T))) %>%
      pivot_longer(
        cols = diabetes:comorbilidad_no_especificada,
        names_to = "tipo_comorbilidad",
        values_to = "n"
      ) %>%
      mutate(
        tipo_comorbilidad = case_when(
          str_detect(tipo_comorbilidad, "diab") ~ "1. Diabetes",
          str_detect(tipo_comorbilidad, "asma") ~ "5. Asma, EPOC o\ntabaquismo",
          str_detect(tipo_comorbilidad, "hipertens") ~ "2. Hipertensión",
          str_detect(tipo_comorbilidad, "renal") ~ "4. Padecimiento renal",
          str_detect(tipo_comorbilidad, "obesid") ~ "3. Obesidad",
          str_detect(tipo_comorbilidad, "tabaquis") ~ "5. Asma, EPOC o\ntabaquismo",
          str_detect(tipo_comorbilidad, "epoc|enfisema") ~ "5. Asma, EPOC o\ntabaquismo",
          T ~ "No se especifica\ncomorbilidad"
        )
      ) %>%
      group_by(resultado, tipo_comorbilidad) %>% summarise(n = sum(n)) %>%
      mutate(prop = n/sum(n)*100,
             tipo = "Base de datos SSalud") %>%
      rename(tipo_confirmacion = resultado)
  ) %>% select(-prop)


fiuf <- "Comparación entre defunciones publicadas por la Secretaría de Salud (federal) y las actas de defunción de la CDMX; desagregación por comorbilidad"
fiuff <- "Aquellas actas que no contuvieran una palabra asociada con 'sospechoso', 'probable' o 'posible', se contabilizaron como casos confirmados."
ggplot() +
  geom_point(data = tempo,
             aes(
               x=n,
               y= tipo_comorbilidad,
               col = tipo
             ), size = 6) +
  ggalt::geom_dumbbell(data = tempo %>% pivot_wider(names_from = tipo, values_from = n),
                       aes(
                         x=`Base de datos SSalud`,
                         xend=`Actas defunción CDMX`,
                         y= tipo_comorbilidad,
                         yend= tipo_comorbilidad
                       ),
                       colour_x="#edae52", colour_xend = "#9fb059",
                       size_x = 6, size_xend = 6,
                       dot_guide = T,
                       colour = "gray", size = 2) +
  scale_x_continuous(breaks = seq(0,1900,200)) +
  facet_wrap(~ tipo_confirmacion) +
  scale_color_manual(name = "", values = c("#edae52", "#9fb059"),
                     labels = c("Base de datos SSalud", "Actas defunción CDMX")) +
  labs(title = str_wrap(fiuf,75),
       subtitle = str_wrap(fiuff,130),
       caption = fiuffi,
       x = "",
       y = "") +
  theme_minimal()+
  theme(plot.title = element_text(size = 32, face = "bold"),
        plot.subtitle = element_text(size = 19),
        plot.caption = element_text(size = 20),
        strip.text = element_text(size = 18),
        panel.spacing.x = unit(3, "lines"),
        text = element_text(family = "Arial Narrow"),
        axis.text.x = element_text(size = 12, angle = 90),
        axis.text.y = element_text(size = 18),
        legend.position = "top",
        legend.text = element_text(size=18))
ggsave(filename = paste0(out, "03.png"),
       width = 15, height = 10, dpi = 100)


tempo <- data_actas %>%
  # mutate(tipo_confirmacion = ifelse(str_detect(tipo_confirmacion, "3"), "1. Confirmado", tipo_confirmacion)) %>%
  mutate(
    comorbilidad_no_especificada = case_when(
      diabetes == 1 ~ 0,
      asma_epoc_tab == 1 ~ 0,
      hipertension == 1 ~ 0,
      renal == 1 ~ 0,
      obesidad == 1 ~ 0,
      T ~ 1
    )
  ) %>%
  select(tipo_confirmacion, diabetes, asma_epoc_tab, hipertension, renal, obesidad, comorbilidad_no_especificada) %>%
  group_by(tipo_confirmacion) %>%
  summarise_all(funs(sum(.,na.rm=T))) %>%
  pivot_longer(
    cols = diabetes:comorbilidad_no_especificada,
    names_to = "tipo_comorbilidad",
    values_to = "n"
  ) %>%
  mutate(
    tipo_comorbilidad = case_when(
      str_detect(tipo_comorbilidad, "diab") ~ "1. Diabetes",
      str_detect(tipo_comorbilidad, "asma") ~ "5. Asma, EPOC o\ntabaquismo",
      str_detect(tipo_comorbilidad, "hipertens") ~ "2. Hipertensión",
      str_detect(tipo_comorbilidad, "renal") ~ "4. Padecimiento renal",
      str_detect(tipo_comorbilidad, "obesid") ~ "3. Obesidad",
      T ~ "No se especifica\ncomorbilidad"
    ),
    tipo = "Actas defunción CDMX"
  ) %>%
  bind_rows(
    data_cov %>%
      mutate(
        comorbilidad_no_especificada = case_when(
          diabetes == "Si" ~ "Si",
          asma == "Si" ~ "Si",
          hipertension == "Si" ~ "Si",
          renal_cronica == "Si" ~ "Si",
          obesidad == "Si" ~ "Si",
          tabaquismo == "Si" ~ "Si",
          epoc == "Si" ~ "Si",
          cardiovascular == "Si" ~ "Si",
          inmusupr == "Si" ~ "Si",
          T ~"No se especifica\ncomorbilidad"
        )
      ) %>%
      select(resultado, diabetes, asma, hipertension, renal_cronica, obesidad, tabaquismo, epoc, comorbilidad_no_especificada) %>%
      mutate_at(.vars = vars(c(diabetes, asma, hipertension, renal_cronica, obesidad, tabaquismo, epoc, comorbilidad_no_especificada)),
                .funs = funs(ifelse(.=="Si",1,0))) %>%
      group_by(resultado) %>%
      summarise_all(funs(sum(.,na.rm=T))) %>%
      pivot_longer(
        cols = diabetes:comorbilidad_no_especificada,
        names_to = "tipo_comorbilidad",
        values_to = "n"
      ) %>%
      mutate(
        tipo_comorbilidad = case_when(
          str_detect(tipo_comorbilidad, "diab") ~ "1. Diabetes",
          str_detect(tipo_comorbilidad, "asma") ~ "5. Asma, EPOC o\ntabaquismo",
          str_detect(tipo_comorbilidad, "hipertens") ~ "2. Hipertensión",
          str_detect(tipo_comorbilidad, "renal") ~ "4. Padecimiento renal",
          str_detect(tipo_comorbilidad, "obesid") ~ "3. Obesidad",
          str_detect(tipo_comorbilidad, "tabaquis") ~ "5. Asma, EPOC o\ntabaquismo",
          str_detect(tipo_comorbilidad, "epoc|enfisema") ~ "5. Asma, EPOC o\ntabaquismo",
          T ~ "No se especifica\ncomorbilidad"
        )
      ) %>%
      group_by(resultado, tipo_comorbilidad) %>% summarise(n = sum(n)) %>%
      mutate(prop = n/sum(n)*100,
             tipo = "Base de datos SSalud") %>%
      rename(tipo_confirmacion = resultado)
  ) %>% select(-prop) %>% filter(!str_detect(tipo_confirmacion, "3"))


fiuff <- "Aquellas actas que explícitamente no definieran si la defunción se trataba de un caso confirmado o sospechoso por COVID-19, fueron descartadas del conteo."
ggplot() +
  geom_point(data = tempo,
             aes(
               x=n,
               y= tipo_comorbilidad,
               col = tipo
             ), size = 6) +
  ggalt::geom_dumbbell(data = tempo %>% pivot_wider(names_from = tipo, values_from = n),
                       aes(
                         x=`Base de datos SSalud`,
                         xend=`Actas defunción CDMX`,
                         y= tipo_comorbilidad,
                         yend= tipo_comorbilidad
                       ),
                       colour_x="#edae52", colour_xend = "#9fb059",
                       size_x = 6, size_xend = 6,
                       dot_guide = T,
                       colour = "gray", size = 2) +
  scale_x_continuous(breaks = seq(0,1900,200)) +
  facet_wrap(~ tipo_confirmacion) +
  scale_color_manual(name = "", values = c("#edae52", "#9fb059"),
                     labels = c("Base de datos SSalud", "Actas defunción CDMX")) +
  labs(title = str_wrap(fiuf,75),
       subtitle = str_wrap(fiuff,115),
       caption = fiuffi,
       x = "",
       y = "") +
  theme_minimal()+
  theme(plot.title = element_text(size = 32, face = "bold"),
        plot.subtitle = element_text(size = 19),
        plot.caption = element_text(size = 20),
        strip.text = element_text(size = 18),
        panel.spacing.x = unit(3, "lines"),
        text = element_text(family = "Arial Narrow"),
        axis.text.x = element_text(size = 12, angle = 90),
        axis.text.y = element_text(size = 18),
        legend.position = "top",
        legend.text = element_text(size=18))
ggsave(filename = paste0(out, "04.png"),
       width = 15, height = 10, dpi = 100)

tempo <- data_actas %>%
  mutate(tipo_confirmacion = ifelse(str_detect(tipo_confirmacion, "3"), "1. Confirmado", tipo_confirmacion)) %>%
  group_by(tipo_confirmacion) %>%
  count(g_edad) %>%
  mutate(tipo = "Actas defunción CDMX") %>%
  bind_rows(
    data_cov %>%
      group_by(resultado) %>%
      count(g_edad) %>%
      bind_rows(
        tibble(
          resultado = c("2. Sospechoso","2. Sospechoso","2. Sospechoso"),
          g_edad = c("0 a 10 años", "11 a 20 años", "21 a 30 años"),
          n = c(0,0,0)
        )
      ) %>%
      rename(tipo_confirmacion=resultado) %>%
      mutate(tipo = "Base de datos SSalud")
  )

fiuf <- "Comparación entre defunciones publicadas por la Secretaría de Salud (federal) y las actas de defunción de la CDMX; desagregado por grupo de edad"
fiuff <- "Aquellas actas que no contuvieran una palabra asociada con 'sospechoso', 'probable' o 'posible', se contabilizaron como casos confirmados."
ggplot() +
  geom_point(data = tempo,
             aes(
               x=n,
               y= g_edad,
               col = tipo
             ), size = 6) +
  ggalt::geom_dumbbell(data = tempo %>%
                         pivot_wider(names_from = tipo, values_from = n),
                       aes(
                         x=`Base de datos SSalud`,
                         xend=`Actas defunción CDMX`,
                         y= g_edad,
                         yend= g_edad
                       ),
                       colour_x="#edae52", colour_xend = "#9fb059",
                       size_x = 6, size_xend = 6,
                       dot_guide = T,
                       colour = "gray", size = 2) +
  scale_x_continuous(breaks = seq(0,900,200)) +
  facet_wrap(~ tipo_confirmacion) +
  scale_color_manual(name = "", values = c("#edae52", "#9fb059"),
                     labels = c("Base de datos SSalud", "Actas defunción CDMX")) +
  labs(title = str_wrap(fiuf,75),
       subtitle = str_wrap(fiuff,130),
       caption = fiuffi,
       x = "",
       y = "") +
  theme_minimal()+
  theme(plot.title = element_text(size = 32, face = "bold"),
        plot.subtitle = element_text(size = 19),
        plot.caption = element_text(size = 20),
        strip.text = element_text(size = 18),
        panel.spacing.x = unit(3, "lines"),
        text = element_text(family = "Arial Narrow"),
        axis.text.x = element_text(size = 12),
        axis.text.y = element_text(size = 18),
        legend.position = "top",
        legend.text = element_text(size=18))
ggsave(filename = paste0(out, "05.png"),
       width = 15, height = 10, dpi = 100)


tempo <- data_actas %>%
  # mutate(tipo_confirmacion = ifelse(str_detect(tipo_confirmacion, "3"), "1. Confirmado", tipo_confirmacion)) %>%
  group_by(tipo_confirmacion) %>%
  count(g_edad) %>%
  mutate(tipo = "Actas defunción CDMX") %>%
  bind_rows(
    data_cov %>%
      group_by(resultado) %>%
      count(g_edad) %>%
      bind_rows(
        tibble(
          resultado = c("2. Sospechoso","2. Sospechoso","2. Sospechoso"),
          g_edad = c("0 a 10 años", "11 a 20 años", "21 a 30 años"),
          n = c(0,0,0)
        )
      ) %>%
      rename(tipo_confirmacion=resultado) %>%
      mutate(tipo = "Base de datos SSalud")
  ) %>% filter(!str_detect(tipo_confirmacion, "3"))

fiuff <- "Aquellas actas que explícitamente no definieran si la defunción se trataba de un caso confirmado o sospechoso por COVID-19, fueron descartadas del conteo."
ggplot() +
  geom_point(data = tempo,
             aes(
               x=n,
               y= g_edad,
               col = tipo
             ), size = 6) +
  ggalt::geom_dumbbell(data = tempo %>% pivot_wider(names_from = tipo, values_from = n),
                       aes(
                         x=`Base de datos SSalud`,
                         xend=`Actas defunción CDMX`,
                         y= g_edad,
                         yend= g_edad
                       ),
                       colour_x="#edae52", colour_xend = "#9fb059",
                       size_x = 6, size_xend = 6,
                       dot_guide = T,
                       colour = "gray", size = 2) +
  scale_x_continuous(breaks = seq(0,900,200)) +
  facet_wrap(~ tipo_confirmacion) +
  scale_color_manual(name = "", values = c("#edae52", "#9fb059"),
                     labels = c("Base de datos SSalud", "Actas defunción CDMX")) +
  labs(title = str_wrap(fiuf,75),
       subtitle = str_wrap(fiuff,130),
       caption = fiuffi,
       x = "",
       y = "") +
  theme_minimal()+
  theme(plot.title = element_text(size = 32, face = "bold"),
        plot.subtitle = element_text(size = 19),
        plot.caption = element_text(size = 20),
        strip.text = element_text(size = 18),
        panel.spacing.x = unit(3, "lines"),
        text = element_text(family = "Arial Narrow"),
        axis.text.x = element_text(size = 12),
        axis.text.y = element_text(size = 18),
        legend.position = "top",
        legend.text = element_text(size=18))
ggsave(filename = paste0(out, "06.png"),
       width = 15, height = 10, dpi = 100)

